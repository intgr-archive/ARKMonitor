#!/usr/bin/env python
import os
import sys
from datetime import datetime

import requests
from bs4 import BeautifulSoup
from dotenv import load_dotenv

load_dotenv()


def send_message(text: str, silent: bool) -> None:
    bot_token = os.getenv('TG_BOT_TOKEN')
    chat_id = os.getenv('TG_CHAT_ID')
    resp = requests.get(
        f'https://api.telegram.org/bot{bot_token}/sendMessage',
        {'chat_id': chat_id, 'parse_mode': 'Markdown', 'text': text, 'disable_notification': silent},
    )
    if resp.status_code != 200:
        print(f"Response: {resp.text}")
    resp.raise_for_status()
    print(f"Message sent: {text}")


def main():
    print(f"{datetime.now()} Starting")
    url = os.getenv('URL')
    current_time = datetime.strptime(os.getenv('CURRENT'), "%d.%m.%Y %H:%M")
    prefer_location = os.getenv('PREFER_LOCATION')

    resp = requests.get(url)
    resp.raise_for_status()
    data = BeautifulSoup(resp.text, 'html.parser')

    table = data.find(id='eksami_ajad:kategooriaBEksamiAjad_data')
    rows = table.find_all('tr')
    available_times = {}
    for row in rows:
        location = row.find('td', {'class': 'eksam-ajad-byroo'}).string
        times = row.find_all('td', {"class": "eksam-ajad-aeg"})
        available_times[location] = [time.string for time in times]

    if not os.path.exists('notified.txt'):
        with open('notified.txt', 'w'): pass

    with open('notified.txt', 'r', encoding='utf-8') as my_file:
        already_notified_dates = my_file.read().splitlines()

    for location, times in available_times.items():
        for time in times:
            date = datetime.strptime(time, "%d.%m.%Y %H:%M")
            notified_txt = f"{location}={time}"
            silent = location != prefer_location
            if date < current_time and notified_txt not in already_notified_dates:
                msg = f"Vaba aeg: {notified_txt}" + (' (s)' if silent else '')
                send_message(msg, silent)

                with open('notified.txt', 'a+', encoding='utf-8') as my_file:
                    my_file.writelines([notified_txt + '\n'])
                    my_file.close()

    sys.exit(0)


if __name__ == '__main__':
    main()
